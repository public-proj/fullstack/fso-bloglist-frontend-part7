<div id="top"></div>
<!--
*** Thanks for checking out the Best-README-Template. If you have a suggestion
*** that would make this better, please fork the repo and create a pull request
*** or simply open an issue with the tag "enhancement".
*** Don't forget to give the project a star!
*** Thanks again! Now go create something AMAZING! :D
-->

<!-- PROJECT SHIELDS -->
<!--
*** I'm using markdown "reference style" links for readability.
*** Reference links are enclosed in brackets [ ] instead of parentheses ( ).
*** See the bottom of this document for the declaration of the reference variables
*** for contributors-url, forks-url, etc. This is an optional, concise syntax you may use.
*** https://www.markdownguide.org/basic-syntax/#reference-style-links
-->

<!-- PROJECT LOGO -->
<br />
<div align="center">

<h3 align="center">Fullstack Open Blog List App</h3>

  <p align="center">
    <br />
    <a href="https://splendid-paletas-bb8da7.netlify.app/">View Demo</a>
    ·
    <a href="https://gitlab.com/public-proj/fullstack/fso-bloglist-frontend-part7/-/issues">Report Bug</a>
    ·
    <a href="https://gitlab.com/public-proj/fullstack/fso-bloglist-frontend-part7/-/issues">Request Feature</a>
  </p>
</div>

<!-- ABOUT THE PROJECT -->

## About The Project

[![](https://i.imgur.com/KXCANBGm.jpg)](https://i.imgur.com/KXCANBG.png) <br/>
[![](https://i.imgur.com/3eu7ZTam.jpg)](https://i.imgur.com/3eu7ZTa.png) <br/>
[![](https://i.imgur.com/065QVdVm.jpg)](https://i.imgur.com/065QVdV.png) <br/>
[![](https://i.imgur.com/1EHY9YVm.jpg)](https://i.imgur.com/1EHY9YV.png) <br/>
[![](https://i.imgur.com/4HABjkQm.jpg)](https://i.imgur.com/4HABjkQ.png) <br/>

### Built With (frontend)

- [![React][react.js]][react-url]
- [![Bootstrap][bootstrap.com]][bootstrap-url]
- [Redux Toolkit](https://redux-toolkit.js.org/)
- [Cypress](https://www.cypress.io/)

### Built With (backend) [Code](https://gitlab.com/public-proj/fullstack/fso-bloglist/-/tree/master)

- [Express](https://expressjs.com/)
- [Mongoose](https://mongoosejs.com/)
- [jest](https://jestjs.io/)
- bcrypt and jwt

<p align="right">(<a href="#top">back to top</a>)</p>

<!-- GETTING STARTED -->

## Getting Started

1. clone the repository - `https://gitlab.com/public-proj/fullstack/fso-bloglist-frontend-part7.git`
2. install dependencies - `yarn install`
3. start the app - `yarn start`

### Prerequisites

This is an example of how to list things you need to use the software and how to install them.

- npm

  ```sh
  npm install npm@latest -g
  ```

- yarn
  ```sh
  npm install --global yarn
  ```

### Installation

Frontend environment variables for running locally

```
REACT_APP_BACKEND_PORT=<BACKEND PORT>
PORT=<FRONTEND PORT>
```

For a remotely hosted backend, edit `API_BASEURL` in `utils/config.js`

Backend env variables

```
DBUSER=<Mongo Atlas username>
DBPASS=<Mongo Atlas user password>
DBNAME=<Mongo Atlas database name>

PORT=<BACKEND PORT>

SECRET=<JWTSecret>

```

<p align="right">(<a href="#top">back to top</a>)</p>

<!-- USAGE EXAMPLES -->

## Usage

### Demo Accounts

Account create is not enabled in the frontend, please use the follow accounts or use the backend API to create a new user
username: demo1, password: demo1
username: demo2, password: demo2

### Backend API Endpoints

`GET /api/blogs/` ------- get all blogs <br/>
`POST /api/blogs/` -------- add a blog (body: title, author, url, likes, userId)<br />
`GET /api/blogs/:id` ------- get a blog by id <br/>
`PUT /api/blogs/:id` -------- edit a blog by id (body: title, author, url, likes)<br />
`PUT /api/blogs/likes/:id` -------- increment likes by blog id <br />
`DELETE /api/blogs/:id` -------- delete a blog by id <br />

`GET /api/comments` ------- get all comments <br/>
`POST /api/comments` ------- post a comment (body: content, blogId)<br/>
`GET /api/comments/:id` ------- get a comment by comment id <br/>
`GET /api/comments/byBlogId/:blogId` ------- get all comments of a particular blog using the blog id<br/>

`POST /api/login` ------- login (body: username, password) <br/>
`GET /api/users` ------- get all users and their statistics <br/>
`POST /api/users` ------ create a user (register) (body: username, name, password, blogs: [])

<p align="right">(<a href="#top">back to top</a>)</p>

<!-- CONTACT -->

## Contact

- Github - [cherylli](https://github.com/cherylli)
- Gitlab - [cherylm](https://gitlab.com/cherylm)
- [@cherylhmurphy](https://twitter.com/cherylhmurphy)

Project Link:
[Frontend](https://gitlab.com/public-proj/fullstack/fso-bloglist-frontend-part7) |
[Backend](https://gitlab.com/public-proj/fullstack/fso-bloglist/-/tree/master)

<p align="right">(<a href="#top">back to top</a>)</p>

<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->

[contributors-shield]: https://img.shields.io/github/contributors/github_username/repo_name.svg?style=for-the-badge
[contributors-url]: https://github.com/github_username/repo_name/graphs/contributors
[forks-shield]: https://img.shields.io/github/forks/github_username/repo_name.svg?style=for-the-badge
[forks-url]: https://github.com/github_username/repo_name/network/members
[stars-shield]: https://img.shields.io/github/stars/github_username/repo_name.svg?style=for-the-badge
[stars-url]: https://github.com/github_username/repo_name/stargazers
[issues-shield]: https://img.shields.io/github/issues/github_username/repo_name.svg?style=for-the-badge
[issues-url]: https://github.com/github_username/repo_name/issues
[license-shield]: https://img.shields.io/github/license/github_username/repo_name.svg?style=for-the-badge
[license-url]: https://github.com/github_username/repo_name/blob/master/LICENSE.txt
[linkedin-shield]: https://img.shields.io/badge/-LinkedIn-black.svg?style=for-the-badge&logo=linkedin&colorB=555
[linkedin-url]: https://linkedin.com/in/linkedin_username
[product-screenshot]: images/screenshot.png
[next.js]: https://img.shields.io/badge/next.js-000000?style=for-the-badge&logo=nextdotjs&logoColor=white
[next-url]: https://nextjs.org/
[react.js]: https://img.shields.io/badge/React-20232A?style=for-the-badge&logo=react&logoColor=61DAFB
[react-url]: https://reactjs.org/
[vue.js]: https://img.shields.io/badge/Vue.js-35495E?style=for-the-badge&logo=vuedotjs&logoColor=4FC08D
[vue-url]: https://vuejs.org/
[angular.io]: https://img.shields.io/badge/Angular-DD0031?style=for-the-badge&logo=angular&logoColor=white
[angular-url]: https://angular.io/
[svelte.dev]: https://img.shields.io/badge/Svelte-4A4A55?style=for-the-badge&logo=svelte&logoColor=FF3E00
[svelte-url]: https://svelte.dev/
[laravel.com]: https://img.shields.io/badge/Laravel-FF2D20?style=for-the-badge&logo=laravel&logoColor=white
[laravel-url]: https://laravel.com
[bootstrap.com]: https://img.shields.io/badge/Bootstrap-563D7C?style=for-the-badge&logo=bootstrap&logoColor=white
[bootstrap-url]: https://getbootstrap.com
[jquery.com]: https://img.shields.io/badge/jQuery-0769AD?style=for-the-badge&logo=jquery&logoColor=white
[jquery-url]: https://jquery.com
