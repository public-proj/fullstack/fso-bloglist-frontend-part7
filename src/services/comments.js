import axios from 'axios';
import config from '../utils/config';

const baseUrl = `${config.API_BASEURL}/api/comments`;

const getCommentsByBlogId = async (blogId) => {
  const response = await axios.get(`${baseUrl}/byBlogId/${blogId}`);
  return response.data;
};

const postComment = async (blogId, content) => {
  // returns commentId
  const response = await axios.post(`${config.API_BASEURL}/api/comments`, {
    blogId,
    content,
  });
  return response.data;
};

export default { getCommentsByBlogId, postComment };
