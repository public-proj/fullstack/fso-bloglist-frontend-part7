import axios from 'axios';
import config from '../utils/config';

const baseUrl = `${config.API_BASEURL}/api/blogs`;

let token = null;

const setToken = (newToken) => {
  token = `bearer ${newToken}`;
};

const getAll = async () => {
  const response = await axios.get(baseUrl);
  return response.data;
};

const likeBlog = async (id) => {
  try {
    const { data } = await axios({
      method: 'put',
      url: `${baseUrl}/likes/${id}`,
    });
    return data;
  } catch (e) {
    console.log(e);
  }
};

const deleteBlog = async (id) => {
  await axios({
    method: 'delete',
    url: `${baseUrl}/${id}`,
    headers: {
      Authorization: token,
    },
  });
};

const postBlog = async ({ title, author, url }) => {
  const newBlog = {
    title,
    author,
    url,
  };

  try {
    const { data } = await axios({
      method: 'post',
      url: baseUrl,
      data: newBlog,
      headers: {
        Authorization: token,
      },
    });

    return data;
  } catch (e) {
    console.log(e);
  }
};

export default { getAll, setToken, postBlog, likeBlog, deleteBlog };
