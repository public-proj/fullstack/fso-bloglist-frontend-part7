import React from 'react';
import PropTypes from 'prop-types';

const LoginForm = ({
  handleSubmit,
  handleUsernameChange,
  handlePasswordChange,
  username,
  password,
}) => (
  <>
    <form onSubmit={handleSubmit} className="form-inline">
      <div className="form-group">
        <label htmlFor="username" className="col-sm-2 col-form-label">
          username
        </label>
        <div className="col-sm-4">
          <input
            className="form-control"
            type="text"
            value={username}
            name="Username"
            id="username"
            onChange={handleUsernameChange}
          />
        </div>
      </div>
      <div className="form-group">
        <label htmlFor="password" className="col-sm-2 col-form-label">
          password
        </label>
        <div className="col-sm-4">
          <input
            className="form-control"
            type="password"
            value={password}
            name="Password"
            id="password"
            onChange={handlePasswordChange}
          />
        </div>
      </div>
      <button type="submit" className="btn btn-primary">
        login
      </button>
    </form>
    <div className="container my-4">
      demo accounts: <br />
      username: demo1, password: demo1 <br />
      username: demo2, password: demo2
    </div>
  </>
);

LoginForm.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  handleUsernameChange: PropTypes.func.isRequired,
  handlePasswordChange: PropTypes.func.isRequired,
  username: PropTypes.string.isRequired,
  password: PropTypes.string.isRequired,
};

export default LoginForm;
