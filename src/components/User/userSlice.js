import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import userService from '../../services/users';

export const fetchAllUsers = createAsyncThunk(
  'users/fetAllUsersStatus',
  async () => {
    return await userService.getAllUsers();
  }
);

export const userSlice = createSlice({
  name: 'user',
  initialState: {
    signedInUser: null,
    allUsers: [],
  },
  reducers: {
    setSignedInUser: (state, action) => {
      state.signedInUser = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(fetchAllUsers.fulfilled, (state, action) => {
      state.allUsers = action.payload;
    });
  },
});

export const { setSignedInUser } = userSlice.actions;

export default userSlice.reducer;
