import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import userService from '../../services/users';
import { fetchAllUsers } from '../../components/User/userSlice';
import { Link } from 'react-router-dom';

const Users = () => {
  //const [users, SetUsers] = useState([]);
  const dispatch = useDispatch();
  const users = useSelector((state) => state.user.allUsers);

  useEffect(() => {
    dispatch(fetchAllUsers());
  }, []);

  return (
    <>
      <table className="table">
        <tr>
          <th></th>
          <th>Blogs created</th>
        </tr>
        {users.map((user) => (
          <tr key={user.id}>
            <td>
              <Link to={`users/${user.id}`}>{user.name}</Link>
            </td>
            <td>{user.blogs.length}</td>
          </tr>
        ))}
      </table>
    </>
  );
};

export default Users;
