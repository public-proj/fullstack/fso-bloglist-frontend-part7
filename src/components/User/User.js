import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router';
import { Link } from 'react-router-dom';
import { fetchAllUsers } from '../../components/User/userSlice';

const User = () => {
  const dispatch = useDispatch();
  const users = useSelector((state) => state.user.allUsers);
  const id = useParams().id;

  if (users.length === 0) {
    dispatch(fetchAllUsers());
  }

  const user = users.filter((user) => user.id === id)[0];

  if (!user) return <h1>Loading...</h1>;

  return (
    <>
      <h3>{user.name}</h3>
      <div>
        <p>added blogs</p>
        <ul>
          {user.blogs.map((blog) => (
            <li key={blog.id}>
              <Link to={`/blogs/${blog.id}`}>{blog.title}</Link>
            </li>
          ))}
        </ul>
      </div>
    </>
  );
};

export default User;
