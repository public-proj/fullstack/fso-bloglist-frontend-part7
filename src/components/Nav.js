import React from 'react';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';

import blogService from '../services/blogs';

const Nav = () => {
  const user = useSelector((state) => state.user.signedInUser);

  const handleLogout = (event) => {
    event.preventDefault();
    window.localStorage.removeItem('loggedBlogAppUser');
    blogService.setToken(null);
    window.location.reload();
  };

  return (
    <>
      <nav className="navbar navbar-expand-lg navbar-light bg-light">
        <Link to="/">
          <a className="nav-item nav-link">Home</a>
        </Link>
        {user && (
          <>
            <Link to="/users">
              <a className="nav-item nav-link">Users</a>
            </Link>
            <div>
              <span>{user.name} logged-in</span>
              <span>
                <button className="btn btn-link" onClick={handleLogout}>
                  Logout
                </button>
              </span>
            </div>
          </>
        )}
      </nav>
    </>
  );
};

export default Nav;
