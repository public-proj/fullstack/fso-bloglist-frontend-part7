import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import blogService from '../../services/blogs';

export const fetchAllBlogs = createAsyncThunk(
  'blogs/fetchAllBlogsStatus',
  async () => {
    return await blogService.getAll();
  }
);

export const postBlog = createAsyncThunk(
  'blogs/createBlogStatus',
  async (blogObject) => {
    return await blogService.postBlog(blogObject);
  }
);

export const deleteBlog = createAsyncThunk(
  'blogs/deteleBlogStatus',
  async (id) => {
    await blogService.deleteBlog(id);
    return id;
  }
);

export const likeBlog = createAsyncThunk('blogs/likeBlogStatus', async (id) => {
  return await blogService.likeBlog(id);
});

export const blogSlice = createSlice({
  name: 'blogs',
  initialState: {
    blogs: [],
  },
  reducers: {
    setBlogs: (state, action) => {
      state.blogs = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(fetchAllBlogs.fulfilled, (state, action) => {
      state.blogs = action.payload;
    });
    builder.addCase(postBlog.fulfilled, (state, action) => {
      state.blogs = [
        ...state.blogs,
        {
          ...action.payload,
          user: { id: action.payload.user },
        },
      ];
    });
    builder.addCase(deleteBlog.fulfilled, (state, action) => {
      state.blogs = state.blogs.filter((b) => b.id !== action.payload);
    });
    builder.addCase(likeBlog.fulfilled, (state, action) => {
      state.blogs = state.blogs.map((b) =>
        b.id === action.payload.id ? { ...b, likes: action.payload.likes } : b
      );
    });
  },
});

export const { setBlogs } = blogSlice.actions;

export default blogSlice.reducer;
