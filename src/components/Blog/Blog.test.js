import React from 'react';
import '@testing-library/jest-dom/extend-expect';
import { render, fireEvent } from '@testing-library/react';
import Blog from './Blog';

describe('<Blog />', () => {
  let component;
  let mockHandler = jest.fn();

  beforeEach(() => {
    const blog = {
      title: 'This is a title - testing with react-testing-library',
      author: 'This is the author',
      url: 'This is the url',
      like: 4123,
      user: {
        id: 1234567,
      },
    };
    component = render(<Blog blog={blog} handleLike={mockHandler} />);
  });

  test('render content', () => {
    const blogDiv = component.container.querySelector('.blog');
    const urlDiv = component.container.querySelector('.blog-url');
    const likeDiv = component.container.querySelector('.blog-likes');

    expect(blogDiv).toHaveTextContent(
      'This is a title - testing with react-testing-library'
    );

    expect(urlDiv).not.toBeVisible();
    expect(likeDiv).not.toBeVisible();
  });

  test('after clicking the button, url and likes are displayed', () => {
    const button = component.getByText('view');
    fireEvent.click(button);

    const urlDiv = component.container.querySelector('.blog-url');
    const likeDiv = component.container.querySelector('.blog-likes');

    expect(urlDiv).toBeVisible();
    expect(likeDiv).toBeVisible();
  });

  test('event handler the component received as props is called twice if the like button is called twice', () => {
    const likeButton = component.getByText('like');
    fireEvent.click(likeButton);
    fireEvent.click(likeButton);

    expect(mockHandler.mock.calls).toHaveLength(2);
  });
});
