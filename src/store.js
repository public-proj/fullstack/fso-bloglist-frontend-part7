import { configureStore } from '@reduxjs/toolkit';
import notificationReducer from './components/Notification/notificationSlice';
import blogsReducer from './components/Blog/blogSlice';
import userReducer from './components/User/userSlice';

export default configureStore({
  reducer: {
    notification: notificationReducer,
    blogs: blogsReducer,
    user: userReducer,
  },
});
